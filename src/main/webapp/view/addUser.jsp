<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<form:form action="/users/add" method="post" modelAttribute="user">
    Name: <form:input path="fullName"/> <form:errors path="fullName" cssClass="error" /><br>
    Email: <form:input path="email"/> <form:errors path="email" cssClass="error" /><br>
    Phone: <form:input path="phone"/> <form:errors path="phone" cssClass="error" /><br>
    Address: <form:input path="address"/> <form:errors path="address" cssClass="error" /><br>
    Birthday: <form:input path="birthday"/> <form:errors path="birthday" cssClass="error" /><br>
    <input type="submit">
</form:form>

</body>
</html>
