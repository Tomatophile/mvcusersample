<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>users</title>
</head>
<body>

<table border="1">
    <caption>Users | <a href="/users/add">Add</a></caption>

    <tr>
        <th>Full name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Address</th>
        <th>Birthday</th>
        <th></th>
        <th></th>
    </tr>

    <c:forEach items="${users}" var="user">
        <tr>
            <td>${user.getFullName()}</td>
            <td>${user.getEmail()}</td>
            <td>${user.getPhone()}</td>
            <td>${user.getAddress()}</td>
            <td>${user.getBirthday()}</td>
            <td><a href="/users/${user.getId()}/edit">edit</a></td>
            <td>
                <form:form action="/users/${user.getId()}/delete" method="DELETE">
                    <input type="submit" value="delete">
                </form:form>
            </td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
