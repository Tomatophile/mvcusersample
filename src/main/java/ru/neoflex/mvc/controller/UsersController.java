package ru.neoflex.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.neoflex.mvc.entity.User;
import ru.neoflex.mvc.repo.UserRepo;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
@RequestMapping("/users")
public class UsersController {
    @Autowired
    UserRepo userRepo;

    @GetMapping
    public String getAllUsers(Model model){
        model.addAttribute("users", userRepo.findAll());
        return "/userList";
    }

    @GetMapping("/add")
    public String addUserForm(Model model){
        model.addAttribute("user", new User().setBirthday(LocalDate.EPOCH));
        return "/addUser";
    }

    @GetMapping("/{id}/edit")
    public String editUserForm(@PathVariable int id, Model model){
        model.addAttribute("user", userRepo.getOne(id));
        return "/editUser";
    }

    @PostMapping("/add")
    public String addUser(@Valid User user, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "addUser";
        }
        userRepo.save(user);
        return "redirect:/users/";
    }

    @PatchMapping("/{id}/edit")
    public String editUser(@Valid User user, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "editUser";
        }
        userRepo.save(user);
        return "redirect:/users/";
    }

    @DeleteMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id){
        userRepo.deleteById(id);
        return "redirect:/users";
    }
}
