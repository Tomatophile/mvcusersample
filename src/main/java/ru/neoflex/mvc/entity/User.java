package ru.neoflex.mvc.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

@Entity
@Table(name = "client")
@Data
@Accessors(chain = true)
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Pattern(regexp = "[a-zA-Z]+[ ]+[a-zA-Z]+", message = "must be full name")
    @Column(name = "fullname")
    private String fullName;

    @Pattern(regexp = "\\w+@[a-z]+[.][a-z]+", message = "email must be valid")
    @Column(name = "email")
    private String email;

    @Pattern(regexp = "[+]79\\d{9}", message = "phone number must be valid")
    @Column(name = "phone")
    private String phone;

    @Pattern(regexp = "г [А-Я][а-я]+, ул [А-Я][а-я]+, д [0-9]+, кв [0-9]+", message = "address must be valid")
    @Column(name = "address")
    private String address;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "date_of_birth")
    private LocalDate birthday;
}
