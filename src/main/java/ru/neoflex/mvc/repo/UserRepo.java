package ru.neoflex.mvc.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.neoflex.mvc.entity.User;

@Repository
public interface UserRepo extends JpaRepository<User, Integer> {
}
