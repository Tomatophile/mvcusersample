CREATE TABLE client
(
    id serial primary key,
    address character varying(255),
    date_of_birth date,
    email character varying(255),
    fullname character varying(255),
    phone character varying(255)
);

insert into client (address, date_of_birth, email, fullname, phone) values ('г Воронеж, ул Моисеева, д 49, кв 62', '2002-03-30', 'tomato@mail.ru', 'Artyom Stroev', '+79192351132');
insert into client (address, date_of_birth, email, fullname, phone) values ('г Воронеж, ул Моисеева, д 49, кв 183', '2002-03-09', 'vinkler@mail.ru', 'Vitaliy Marchenko', '+79913242940');
