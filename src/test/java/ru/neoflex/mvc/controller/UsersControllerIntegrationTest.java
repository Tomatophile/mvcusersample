package ru.neoflex.mvc.controller;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.neoflex.mvc.repo.UserRepo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@Testcontainers
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@AutoConfigureMockMvc(printOnlyOnFailure = false)
@ContextConfiguration(initializers = {UsersControllerIntegrationTest.Initializer.class})
class UsersControllerIntegrationTest {
    @Container
    static final PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:11")
            .withInitScript("testdb.sql");

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private UserRepo userRepo;

    @Test
    @Order(1)
    void getAllUsers_ReturnsUserListView() throws Exception {
        // when
        this.mockMvc.perform(get("/users/"))
                // then
                .andExpect(status().isOk());
    }

    @Test
    @Order(2)
    @Transactional
    void addUser_ReturnsRedirectToGetAllUsers() throws Exception {
        // when
        this.mockMvc.perform(post("/users/add/")
                .param("fullName", "Ivan Ivanov")
                .param("email", "suslik@mail.ru")
                .param("phone", "+79511245240")
                .param("address", "г Пенза, ул Ленина, д 30, кв 11")
                .param("birthday", "1997-09-07"))
                .andExpect(status().is3xxRedirection());

        var user = userRepo.getOne(3);

        Assertions.assertEquals("+79511245240", user.getPhone());
    }

    @Test
    @Order(3)
    @Transactional
    void editUser_ReturnsRedirectToGetAllUsers() throws Exception {
        // when
        this.mockMvc.perform(patch("/users/1/edit/")
                .param("fullName", "Artyom Stroev")
                .param("email", "tomato@mail.ru")
                .param("phone", "+79192351132")
                .param("address", "г Пенза, ул Ленина, д 30, кв 11")
                .param("birthday", "2002-03-30"))
                .andExpect(status().is3xxRedirection());

        var user = userRepo.getOne(1);

        Assertions.assertEquals("г Пенза, ул Ленина, д 30, кв 11", user.getAddress());
    }

    @Test
    @Order(4)
    void deleteUser_ReturnsRedirectToGetAllUsers() throws Exception {
        // when
        this.mockMvc.perform(delete("/users/2/delete/"))
                .andExpect(status().is3xxRedirection());

        var users = userRepo.findAll();

        Assertions.assertTrue(users.size()<3);
    }

    static class Initializer
            implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }
}
